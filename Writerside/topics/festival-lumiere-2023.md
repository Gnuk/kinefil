# Festival Lumière 2023

## Les Aristocrates

Le film raconte le rapport de dignité d'un père aristocrate qui voit petit à petit les traditions familiales se perdre.

## Le Chemin

Un film centré sur Daniel surnommé "le hibou" et ses amis pendant son enfance, l'action est vue de leur prisme pour amplifier le rapport social de cette Espagne rurale d'après guerre.

## Boulevard du crépuscule

Un classique qui commence par une affaire de meurtre avant de ce centrer sur une actrice autrefois célèbre.

## L'Exorciste

J'ai "étrangement" pensé à ces quelques mots d'Edith Piaf "Tu me fais tourner la tête" mais pas pour les mêmes raisons.

## Sans pitié

Une histoire d'amour entre un soldat afro-américain et une jeune femme à la recherche de son frère. Le film fonctionne, le sujet reflète l'époque d'après-guerre tout en questionnant l'actualité.

## Un Silence

Un père avocat se retrouve être la cible d'une affaire judiciaire de famille. Le film raconte la façon dont les membres de la famille vont devoir traverser cette épreuve. Après Les Intranquilles, Joachim Lafosse, revient avec un film tout aussi complexe, sur des sujets tabous de société.

## Winter Break

Un excellent film de Noël qui n'en fait pas des caisses.

## Bushman

De magnifiques plans dans cette expérimentation un peu décousue sur Gabriel, un Nigérien qui arrive aux États-Unis pendant la fin des années 60.

## Le Privé

Un film palpitant de Robert Altman sur un détective privé qui se retrouve au cœur d'une affaire de meurtre.

## Let's Get Lost

Un excellent documentaire sur la vie du trompettiste Chet Baker. Une grande partie de l'équipe du film était là dont Bruce Weber son réalisateur.

## La Merveilleuse Histoire de Henry Sugar

Le dernier court métrage de Wes Anderson, je le préfère à son dernier long métrage _Asteroid City_.

## The Grand Budapest Hotel

Sans doute le meilleur film de Wes Anderson, si vous ne l'avez pas encore vu, allez le voir.

## Le Cercle des neiges

Je m'attendais à un film très sensationnaliste au vu de la bande-annonce, il est plutôt d'une grande humanité et d'une excellente mise en scène. Le film a beaucoup de respect pour ses personnages face à cette histoire retranscrite sur ce crash d'avion déjà adapté plusieurs fois au cinéma. À voir absolument.

## Le Garçon et le Héron

Le dernier film de Miyazaki est fidèle à ses précédents, ne révolutionne pas son genre, n'est sans doute pas un bon choix pour commencer son cinéma, mais contient presque tout ce qu'on peut attendre du réalisateur.

## Les Princes de la ville

Un excellent film de Taylor Hackford autour de guerres de clans et focalisé sur le rapport entre trois amis qui vont se séparer au fil des années. Le film est long, mais on ne le voit pas passer.

## Paris, Texas

Formidable film de Wim Wenders avec une photographie magnifique de Robby Müller.

## L'Homme au coin du mur rose

Le film n'est pas inintéressant, mais il se perd dans sa narration et sa réalisation.

## Portraits fantômes

Un beau portrait nostalgique de la ville de Recife au Brésil avec un passage sur ses cinémas.

## Le Cabinet du Docteur Caligari

C'est toujours intéressant de découvrir des grands classiques du cinéma muet accompagnés d'un orchestre, d'autant plus quand ledit film a tant inspiré le cinéma de genre.

## M\*A\*S\*H

Ce n'est pas le meilleur film d'Altman à mon sens. Il reste très bien mis en scène malgré quelques scènes de chirurgies peu ragoutantes, mais aussi peu réalistes.

## La Bête humaine

Un classique avec Jean Gabin qui joue un conducteur de train. C'est l'adaptation du livre homonyme d'Émile Zola et c'est réalisé par Jean Renoir.

## Les Ailes du désir

Le film et son sujet sont passionnants même si j'ai une préférence pour sa suite "Si loin, si proche !".

## Le Grand duel

Un très bon western avec Lee Van Cleef.

## Les Moissons du ciel

Ce film est formidable, j'ai été captivé par sa mise en scène, son image, son sujet. Un très grand film.

## Il était un père

Je découvre Ozu par ce film. J'ai eu un peu de mal avec son rythme même si le film est très bien.

## The Player

Une formidable critique du cinéma hollywoodien dans ce film centré sur un directeur de production.

## L'Ami américain

Suite à sa rencontre avec Tom, un Américain, Jonathan va se retrouver dans des affaires sombres.

## Le Pot d'un million de ryō

Un film autour d'un pot qui vaut un million de ryō, sa vente, sa recherche, … C'est pour moi la bonne surprise du Festival, le film est drôle, bien rythmé, bien mis en scène et avec plein d'idées.

## Cookie's Fortune

Une très belle mise en scène et un très bon rythme pour ce film sur la fortune de Cookie suite à sa mort et les rapports entre ses proches.

## L'Échine du diable

Je ne pensais pas avoir déjà vu ce film quand j'étais plus jeune, très bonne redécouverte de ce film de Guillermo del Toro.

## Enter the Void

Assez inclassable dans sa photo, dans ses mouvements de caméras, je suis toujours absorbé par le film. Assez singulier, pas forcément accessible, il n'en reste pas moins une sacrée expérience de cinéma.

## La Classe américaine

Le film est toujours très drôle malgré certains passages qui ont du mal à passer aujourd'hui.

## Le Nom de la rose

Une enquête sur des meurtres dans une abbaye. J'ai plutôt aimé le film.
