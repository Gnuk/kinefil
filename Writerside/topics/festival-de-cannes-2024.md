# Festival de Cannes 2024

## Napoléon vu par Abel Gance - première période

J'avais beaucoup d'attentes sur ce film de 7h au total dont la première période projetée à Cannes a durée 3h47. Aucune déception, je suis surpris par la qualité du film que ce soit en terme de rythme, d'humour, de photographie, de construction des plans, de l'utilisation de la transparence et du travail sur la musique. Je ne m'attendais pas à une telle qualité, je ne me suis pas ennuyé et j'avais envie de voir la suite. La seule réponse que je n'aurais pas c'est sur la projection de la partie triptyque qui est censée durer 20 minutes dans le film et dans laquelle ce n'est plus une seule image (1.33, soit 4:3) mais 3 projections cote à cote : ce qui donne une image d'un ratio de 4:1. J'ai hâte de voir la deuxième partie, espérons le dans des conditions adaptées au triptyque.

## Le deuxième acte

Dernier Dupieux en ouverture du festival, de beaux travellings, j'ai ri.

## Les Sept Samouraïs

Un grand classique de Kurosawa magnifiquement restauré.

## Red Road

Une veilleuse de nuit se retrouve à traquer un homme qu'elle ne voulait plus jamais revoir. Le film fonctionne, il est assez classique dans son genre au permet abord mais dévoile toute sa complexité dans son dernier acte.

## Ma vie, ma gueule

Dernier film avant la mort de sa réalisatrice, très intimiste, il restera pour moi malheureusement très oubliable.

## Les Fantômes

Une histoire de traque de criminels de guerre syriens. Le film est bien écrit, il fonctionne bien, à voir.

## It Doesn't Matter

Les phases d'animations et quelques fulgurances de très bonne qualité ne parviennent pas à rendre le film moins foutraque et ça tire en longueur.

## Diamant Brut

T'es belle mise en scène accompagnée de l'excellente prestation de Malou Khebizi qui encre dans le réel des propos pourtant basés sur l'apparence et la volonté de notoriété.

## When the Light Breaks - Ljósbrot

Un film sur le deuil, avec un fond intéressant, la forme reste tout de même très convenue ce qui rend le film oubliable.

## La Jeune femme à l’aiguille

J'ai bien aimé la thématique sur les abandons de nouveaux nés dans ce film assez gros sabots, avec des faiblesses sur la caractérisation de ses personnages secondaires et d'autres moments sur sa narration. Cependant son noir et blanc le sublime.

## Bird

Un film qui se déroule à travers une fille de 12 ans dans un contexte familial compliqué. Un jour elle va rencontrer Bird. L'utilisation du fantastique fonctionne dans ce contexte social compliqué.

## Megalopolis

Le film est dense, très inégal, il a des fulgurances, il est peu lisible. Graphiquement, il varie beaucoup entre des plans avec une esthétique douteuse et d'autres très bien composés. Je ne sais pas encore quoi penser du film.

## Twilight of the Warriors : Walled In

Un bon moment de détente, loin d'être aussi bien que son Limbo, il reste cependant un excellent spectacle.

## Les Damnés

Un film très contemplatif dont l'intrigue se déroule pendant la guerre de Sécession.

## On Becoming a Guinea Fowl

Le film commence sur une séquence absurde pour basculer dans une deuxième partie plus dramatique. Ça parle de pintade aussi. J'ai du mal à comprendre les choix du film pour déverser son message.

## The Surfer

Un film dans lequel Nicolas Cage joue quelqu'un qui veut faire du surf avec son fils. C'était en séance de minuit et je l'ai vu le lendemain, ça fonctionne bien, c'est très bien rythmé. À voir.

## Oh, Canada

Le film est centré sur le témoignage d'un cinéaste qui a eu un cancer. C'est très verbeux, ça raconte plus que ça montre. J'ai déjà oublié le film.

## The Shameless

Le film est une très bonne surprise, il aborde des sujets difficiles autour de la prostitution et du pouvoir.

## Trois kilomètres avant la fin du monde

Un jeune homme se fait agresser dans la rue, ce que sa famille va découvrir va bouleverser tout son village. Le film est bien scénarisé, bien mis en scène et son sujet est efficace et très dur.

## Kinds of Kindness

Trois histoires absurdes de Lánthimos qui nous a habitué à mieux dans son cinéma.

## Armand

Des témoignages s'affrontent entre professeurs et parents pour expliquer ce qu'il s'est passé entre des élèves. Ça va créer des situations complexes. J'aime bien l'idée que le film se concentre sur les adultes, c'est efficace et il y a un moment assez particulier dans le film avec Elisabeth qui justifie à lui seul d'aller voir le film.

## Caught by the Tides

Je suis assez hermétique à ce genre de films, les transitions sont assez brutales, l'histoire est très confuse tout en restant simple, c'est très contemplatif.

## Emilia Perez

Une histoire autour d'une avocate et un barron de la drogue qui veut changer de genre. Très belle mise en scène de Jacques Audiard qui place les chants pour sublimer les émotions que traversent ses personnages.

## Rumours

7 presidents, un gros cerveau, une brouette, ce sont trois éléments que vous retrouverez dans ce film entre la satire et le fantastique.

## Les reines du drame

Premier long métrage d'Alexis Langlois dont j'avais déjà vu un clip pour le festival Écrans Mixtes. J'avais hâte de découvrir ce film pop et over the top. Sa mise en scène est en opposition avec les codes classiques du cinéma. C'était virement un excellent moment.

## Limonov: The Ballad of Eddie

Le dernier film de Kirill Serebrennikov reste maîtrisé sur ce poète engagé qui voyage entre Moscou et New-York. En revanche, il ne dépasse ni n'égale ses 3 précédents longs métrages.

## My Sunshine

Un très beau film centré sur une relation coach élève dans le milieu du patinage artistique.

## C'est pas moi

N'oubliez pas la scène post-générique, elle justifie à elle seule le visionnage de ce court métrage.

## The Substance

Un film de genre sur le rapport au corps, la beauté, la célébrité et ses enjeux aux yeux du grand public. Une bonne mise en scène avec des choix scénaristiques affirmés.

## Les femmes au balcon

Il fait chaud, c'est l'été, des amies se retrouvent au balcon dans une ambiance "fenêtre sur cour" à première vue. Le film bascule dans un tout autre genre avec rythme, humour et légèreté malgré les thématiques abordées.

## Santosh

Une femme devient policière et enquête sur la mort d'une jeune fille. Le film profite de sa mise en scène pour montrer un point de vue assez dur sur son sujet.

## Le Royaume

La Corse, ses clans, ses règlements de comptes. Le film met bien en image l'escalade de la violence.

## Les Linceuls

Le film, malgré quelques passages esthétiques, se morfond dans la vanité de ses dialogues verbeux.

## Anora

Une travailleuse du sexe rencontre un fils pourri-gâté Russe, sa vie va changer. Le film est bien rythmé, bien réalisé, c'est aussi très drôle et avec beaucoup d'action.

## Marcello Mio

Un film sur Marcello, enfin plutôt autour de son entourage qui joue dans le film. Je trouve le film complaisant et nombriliste. Au final, ça n'est pas aussi centré que ça sur Marcello Mastroianni.

## Parthenope

C'est bien réalisé et bien rythmé, la photographie est très belle, mais au final ça ne s'adresse pas à beaucoup de monde puisque c'est très centré sur Naples.

## Maria

La très bonne interprétation d'Anamaria Vartolomei n'arrive pas à sauver l'écriture qui manque de consistance malgré la terrible histoire de Maria Schneider.

## L'histoire de Souleymane

Une histoire sur un sans-papier qui essaie de s'en sortir à Paris. Une bonne interprétation et réalisation. 

## Grand Tour

Passé la première heure et demie, la demi-heure restante est bien rythmée. Le noir et blanc est beau.

## Viet and Nam

Un couple de mineurs, un père, un shaman, un champ de mine, un conteneur. C'est lent.

## Le roman de Jim

Une belle histoire autour d'une composition familiale servie par de bons acteurs : c'est rafraichissant et dramatique à la fois et ça fonctionne très bien tout au long du film.

## Motel destino

Un délinquant vient travailler dans un Motel érotique pour échapper à ses agresseurs. C'est très plaisant à regarder, bien rythmé, efficace, mais ça ne révolutionne pas le genre.

## Vivre, Mourir, Renaître

J'ai beaucoup apprécié la façon dont est traité le film malgré quelques moments un peu trop rapide dans l'intrigue. Au fur et à mesure, la consistance arrive et nous laisse un bon film dans ensemble.

## Le Comte de Monte-Cristo

Je ne m'attendais à rien à cause des opus récents sur les 3 mousquetaires qu'il surpasse en tout point. Même si le démarrage est un peu convenu, l'histoire et sa mise en scène devient progressivement passionnante.

## All we imagine as light

Une histoire entre des femmes qui vivent ensemble et leurs compagnons. C'est ennuyeux et ça manque de rythme.

## L'Amour ouf

Une relation passionnelle entre un bandit et une jeune fille sur fond de violence. Le film est à la fois drôle, prenant et émouvant. Ça fonctionne avec sa mise en scène dynamique via ses travellings originaux ainsi que son scénario bien écrit.

## The seed of the sacred fig

À Téhéran, un policier, sa femme et ses deux filles, une arme. C'est un excellent film qui évoque à travers la fiction (ainsi que de réels passages au smartphone) une horrible réalité.

## Angelo dans la forêt mystérieuse

Un film d'animation pour enfant assez lambda et avec plein de défauts. Je l'ai très vite oublié.

## La Plus Précieuse des marchandises

Un film très touchant entre cet enfant et ces bûcherons ainsi que le regard des autres. Il ne révolutionne pas l'animation, reste très simple et efficace.

## She's Got No Name

Meme si le film met du temps à démarrer et a quelques répétitions, il fonctionne plutôt bien et devient passionnant au fur et à mesure du visionnage.

## Flow
Ça film contemplatif d'animation sur un chat qui traverse de manière onirique arrive à nous emporter au début mais ses failles techniques m'ont un peu sorti du film.
