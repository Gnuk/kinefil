# Festival de Cannes 2023

## Jeanne du Barry

Un film qui parle de l'histoire de Jeanne du Barry avec sa relation avec le roi. La mise en scène est assez classique, ça manque de profondeur dans le scénario.

## Ama Gloria

L'ouverture de la Semaine de la Critique, une histoire entre Cléo, une jeune fille de 6 ans et sa nounou Gloria. Cléo va partir la rejoindre au Cap-Vert. Le film est très classique dans son genre. L'interprétation de Cléo est plutôt bonne.

## Tiger Stripes

Ça parle d'une fille qui vit sa transformation d'adolescente en utilisant les codes du genre. C'est sans doute le film que j'ai le moins apprécié dans le festival parce qu'il tente de mélanger le cinéma de Julia Ducournau et d'Apichatpong Weerasethakul sans pour autant le rendre consistant. Bref, une mixture indigeste en ressort et je ne comprends pas pourquoi le film a gagné la Semaine de la Critique.

## Anselm

Un film en 3D qui retrace l'œuvre d'un artiste. C'est plutôt beau, mais le chapitrage exposé comme une visite de musée me semblait pompeux jusqu'à ce que ça prenne plus d'ampleur dans sa seconde partie. Une expérience intéressante avec quelques longueurs.

## Le Règne Animal

L'ouverture de la compétition Un certain regard. Le film traite des relations avec des humains et des personnes qui se transforment en animaux à cause d'une maladie récente et méconue. Le film est très bon bien que l'épilogue soit peut-être de trop. Les effets spéciaux marchent très bien, c'est chouette à voir dans le cinéma français.

## Simple comme Sylvain

C'est une comédie à propos d'une histoire d'amour entre deux personnes de milieux sociaux différents. C'est à la fois drôle et touchant, ça explore bien le sujet, j'ai passé un très bon moment devant ce film.

## Linda veut du poulet !

Un dessin animé assez minimaliste avec une histoire qui a une forte profondeur et qui n'hésite pas à remettre en question des problématiques de société actuelles. Très bonne surprise pour ce film qui a pourtant un titre pas très aguicheur. À voir, vous allez passer un bon moment.

## Perdidos en la noche

Une histoire autour d'une disparition de la mère du personnage principal qui va enquêter à sa manière pour comprendre ce qu'il s'est passé. C'est assez inégal, el début est plutôt bon et on arrive à une résolution qui ne fonctionne pas, dommage.

## Rosalie

Une histoire avec et autour de Rosalie, une femme à barbe. Ça tire un peu en longueur, il y a de bonnes idées qui me semblent pas assez approfondies parfois, un peu déçu même si ça reste agréable à regarder.

## The sweet east

Une fille qui s'évade dans les États-Unis d'Amérique pour rencontrer des groupes assez extrêmes. Une excellente photographie et un traitement du sujet magnifiquement mis en scène. C'est pour moi un gros coup de cœur et une très bonne satire.

## Black Flies

Un sujet autour du travail des ambulanciers aux US. Un film plutôt divertissant, mais avec un manque de profondeur. Une vision peut-être trop centrée sur les vices, la drogue et la précarité des patients.

## Déserts

Une histoire de deux agents de recouvrement qui vont avoir bien du mal à se faire rembourser. C'est très drôle dans les petites scénettes dans la première partie et ça devient plus contemplatif dans la seconde. Cette rupture bien que brutale n'entache en rien l'excellente photo du film.

## Monster

Une histoire qui se passe dans le milieu scolaire entre un enseignant, des élèves, la directrice. Le film aborde de nombreuses thématiques autour de ce milieu et s'évade dans une deuxième partie. J'ai adoré ce film pour à la fois son scénario et sa mise en scène. À voir, c'est très bien.

## Omar la fraise

Un ancien bandit français est contraint d'aller en Algérie pour sa nouvelle vie. On dirait plus un film à sketch qu'un film avec un vrai scénario, je ne suis, semble-t-il pas la bonne cible pour le voir.

## Vincent doit mourir

Vincent se fait attaquer par un de ses collègues dans l'entreprise dans laquelle il travaille. Il va découler un enchaînement d'événements suite à cette action. Malgré quelques petits défauts, c'est un excellent film qui explore admirablement le genre.

## Les filles d'Olfa

En Tunisie, une mère de 4 filles raconte la disparition de ses deux filles ainées. Le film est entre le documentaire et la fiction. J'ai trouvé que le film ne marchait pas avec moi puisqu'il ne choisit pas entre le documentaire et la fiction malgré de belles compositions.

## Conann

L'histoire des six vies de Conann. Un film un peu fouillis, toujours dans le style de Mandico, mais sans doute trop. J'ai préféré à After Blue mais ça ne va, à mon sens, pas au niveau des garçons sauvages. Je trouve le film trop difficilement accessible pour ceux qui ne connaissent pas déjà son cinéma.

## Il pleut dans la maison

Une histoire sur un frère et une sœur qui tentent de se débrouiller seul. Le sujet a déjà été vu de nombreuses fois et son traitement ressemble à celui d'un film de fin d'année d'étudiant en cinéma. Bref, j'ai pas aimé.

## The Feeling That The Time For Doing Something Has Passed

C'est l'histoire d'une femme qui pratique le BDSM avec un partenaire, puis elle expérimente d'autres partenaires. Le film est en dessous de mes attentes, c'est un drame plutôt classique sous-teinté de BDSM.

## Robot Dreams

Une histoire d'amitié entre un chien et un robot. Malheureusement, ils vont devoir se séparer. Très bon film sur l'amitié, tout est raconté par l'image.

## The Zone of Interest

La vie de famille autour d'un commandant d’Auschwitz dans une enclave à côté du camp. Le concept est excellent, en revanche, le film aurait pu durer moins ou plus de temps, le message aurait été exactement le même.

## May December

Une actrice va jouer le rôle d'une femme qui s'est mariée avec un homme de 23 ans de moins qu'elle. J'ai trouvé ce film très moyen, pas forcément adapté à de la compétition.

## Banel et Adama

Banel et Adama s'aiment malgré les conventions de leur communauté au Sénégal. C'est un film plutôt classique sans son scénario, mais qui fonctionne malgré tout.

## Project Silence

Un film d'action avec du brouillard et des chiens. Alors, ce n'est pas très intéressant, c'est du scénario catastrophe qui s'épuise dans son action.

## Le Syndrome des amours passées

Un couple qui n'arrive pas à avoir d'enfant va suivre une nouvelle thérapie pour en avoir. Très drôle, les scènes de sexe sont très inventives, une bonne surprise du festival.

## Only the river flows

Une enquête suite à des meurtres va semer des doutes chez l'inspecteur. J'ai décroché du film plusieurs fois, je trouve que le film n'est pas très bien construit malgré une photo que j'ai bien aimé.

## Le Livre des solutions

Le dernier Gondry, c'est drôle, allez le voir.

## Firebrand - Le Jeu de la reine

L'histoire est centrée sur la femme d'Henri VIII. Ce dernier a pour habitude de répudier voire d'exécuter ses épouses. Le film est plutôt bien dans l'ensemble même s'il reste un peu classique.

## Anatomie d'une chute

Un homme va mourir suite à une chûte dans des circonstances inconnues, sa femme sera suspectée d'homicide. Le scénario écrit par Justine Triet et Arthur Harari fonctionne merveilleusement bien dans ce film à voir absolument.

## Club Zero

Une professeure dans un lycée va entraîner ses élèves dans des cours de nutrition assez particuliers. J'ai adoré le film qui est d'un cynisme digne de ce qu'il se passe malheureusement dans la réalité.

## Les Feuilles mortes

Une histoire d'amour et une suite de malentendus. Un film que je qualifie de "bonbon", on passe un bon moment, le film est tout simple, à une photo magnifique et fonctionne très bien.

## Le Théorème de Marguerite

Marguerite est une brillante élève en Mathématiques à l'ENS. Suite à une erreur, son destin va basculer. Un film très simple, très attendu mais qui est assez revigorant et divertissant. À placer pour faire une pause entre deux films.

## Kubi

Une histoire de samouraï qui part dans tous les sens mais qui est très drôle. J'ai bien aimé.

## Asteroid City

J'aimerais bien vous dire de quoi ça parle mais vous verrez que c'est finalement très vide puisque le film se concentre plus sur son placement d'acteurs que sur une histoire. Extrêmement déçu malgré une photo et une mise en scène impeccable.

## Rapito (L'enlèvement)

Un jeune enfant juif se fait baptiser dans le dos de ses parents. Il va alors être enlevé par l'Église pour son éducation. J'ai trouvé le film magnifiquement exécuté et le sujet très prenant, je recommande.

## Crowrã

L'histoire de la persecution d'une communauté indigène dans une forêt du Brésil. Le sujet est fort mais le film qui vacille entre documentaire et fiction peine à démarrer, est inconsistant, dommage vu le sujet.

## La Fille de son père

Suite à la disparition de sa mère, le père se voit contraint de s'occuper de sa fille, il deviendra très présent dans sa vie. Un film qui avait pourtant une première scène d'introduction intéressante devient progressivement un mauvais film durant son déroulement.

## La Passion de Dodin Bouffant

Une histoire de cuisine avec Dodin et sa cuisinière Eugénie. Le film est très contemplatif sur la nourriture, le scénario est simple mais la mise en scène est efficace. Le titre anglais est parfait : "The Pot-au-Feu".

## Vers un avenir radieux

Un réalisateur tourne son nouveau film, il va devoir gérer diverses problématiques autour de lui. Le film aborde différents sujets, il peine à choisir, ce qui rend le message difficile à saisir.

## Perfect Days

Le film est centré sur la vie simple de quelqu'un qui entretient les toilettes publiques de Tokyo. J'ai trouvé le film très réussi, le rythme bien que lent est efficace pour nous montrer le quotidien de son protagoniste et les petits bonheurs qu'il va rencontrer. Le prix d'interprétation est amplement mérité.

## Salem

Une histoire autour d'un jeune homme au centre d'une guerre de clans à Marseille. Le film a plein de bonnes idées et mêle très bien le fantastique à l'histoire, bien qu'il ait quelques faiblesses, c'est à voir.

## Dans la toile

Un réalisateur va se battre pour mener à bout la réalisation de son premier film. Avec une thématique proche de "Vers un avenir radieux", j'ai trouvé "Dans la toile" plus réussi, le film est entraînant, bien construit, les situations sont fortes, j'ai passé un très bon moment.

## L'Été dernier

Une histoire d'amour entre une avocate et son beau fils de 17 ans. Le film arrive avec ses gros sabots et des situations globalement mal amenées, ce qui renforce la gêne et le ridicule. Un film raté malgré un sujet passionnant. Certain diront que c'est la préquelle de May December.

## La Chimère

Un pilleur de tombe va œuvrer avec ses amis brigands. Le film est assez bien construit bien que difficile à accorcher au début. Certains petits éléments viennent enrichir le film, comme la baguette de sourcier. C'est assez classique mais plutôt agréable à voir, d'autant plus que la photo et les changements de ratio d'image sont travaillés.

## Une nuit

Un film de dialogue entre deux personnes, qui, le temps d'une nuit, vont réfléchir sur leur perception de l'amour. Le film, bien que très peu visuel passionne par son sujet.

## Hypnotic

Un flic va enquêter sur la disparition de sa fille, il va être confronté à un homme au comportement étrange. Vous verrez, le film s'inspire de ce qui est déjà fait dans des grands films du cinéma sans les égaler, ce qui donne un film de série B assez lambda au final.

## L'Abbé Pierre : Une vie de combats

Une retrospective de la vie de l'Abbé Pierre. On qualifie souvent un film de "film wikipédia" lorsqu'on peut se contenter de lire sa fiche. Ce film en est un exemple : ça manque d'émotions, de profondeur.

## The Old Oak

Un pub au Royaume-Uni, "The Old Oak" va accueillir des réfugiés syriens dans un contexte social difficile. Je connais peu le cinéma de Ken Loach mais je n'ai pas eu l'impression de voir quelque chose de nouveau par rapport au seul autre film que j'ai vu de son auteur : Moi, Daniel Blake.

## Élémentaire

Des personnages faits de feu, d'eau, de terre et d'air vivent ensemble dans une ville. Un jour une fille de l'élément feu va rencontrer un garçon de l'élément eau. Le film bien que très sympathique n'est pas au niveau d'un Vice-Versa.

## L'amour et les forêts

Une histoire d'emprise au sein d'un couple. J'ai rattrapé ce film qui est globalement bien en rentrant du festival. Les acteurs sont bons, l'histoire est prenante et pleine de tensions, on ressent la difficulté de la relation. Je reste cependant persuadé qu'il manque quelque chose pour le rendre encore plus percutant.