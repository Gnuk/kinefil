# Festival Lumière 2022

## Vive le Tour !

Synopsis : Un petit documentaire de Louis Malle qui parle du Tour de France 1962.

Ce qui est bien avec ce documentaire, c'est qu'il est bien rythmé, il parle de nombreux sujets autour du tour et c'est assez chouette de voir ça avec le recul.

## Les Amants

On voit tout de suite que Louis Malle arrive à mettre en scène son actrice principale dans ses diverses péripéties entre Paris et Dijon.


## L'Innocent

Même si le jeu est bon, la mise en scène fonctionne, je n'arrive pas à apprécier l'humour décalé du film qui me donne l'impression de cacher un drame derrière son ressort comique.

## Lost Highway

Lost Highway est une histoire dans laquelle Lynch nous perd dans ses rêves pour un moment de cinéma unique. Et dire qu'il fera Mulholland Drive 4 ans plus tard.

## Mademoiselle Docteur

Une curiosité centrée sur une espionne, Mademoiselle Docteur tire un peu en longeur et se perd un peu dans ses intrigues.

## Un homme comme tant d'autres

Le film nous parle de la discrimination dans les années 60 au travers du périple du protagoniste afro-américain. Même si l'image, le montage, la mise en scène sont bonnes dans son ensemble, le film me laisse avec un propos qui me semble en dehors de l'idée initiale. Je serai ravi d'en discuter avec ceux qui ont vu le film.

## Bardo, fausse chronique de quelques vérités

Un excellent film dans lequel s'interroge le cinéaste à travers de nombreuses allégories, peut-être un peu long mais un bon moment de cinéma. Ça vaut le coup de le voir sur grand écran.

## Douze hommes en colère

Un grand classique qui fonctionne toujours. Un jury de 12 hommes autour d'un procès. Pas une seconde n'est à enlever dans le film. Et dire que c'est le premier film de Lumet.

## Le Voyage fantastique

Alors, pour moi ça a mal vieilli, ça fait un peu nanar, on voit tous les effets. Ça a un certain charme mais c'est bourré de clichés. Et dire qu'il a eu un oscar pour ses effets visuels à l'époque, c'est là qu'on voit la progression.

## The Wondar

Un film avec une photo qui rappelle "La leçon de piano", c'est beau, c'est bien mis en scène, on s'attache, le film est prenant. Encore un bon film que vous allez retrouver sur Netflix alors que le grand écran le met tellement en valeur.

## Dementia

Un film à part qui sera la seule réalisation de son auteur avec de vraies volonté de cinéma autour d'un revenge movie qui fonctionne et qui est bourré d'idées.

## Certains l'aiment chaud

Je ne m'attendais pas à ça, si vous ne l'avez pas vu, foncez, c'est sans doute la meilleure surprise que j'ai eu dans le festival.

## Frankenweenie

Très sympa sur le début mais souffre des mêmes problèmes que je trouve dans beaucoup de films de Tim Burton, cette volonté à vouloir trop en faire et ainsi effacer son récit au profit du divertissement.

## Les Noces de Toprin

Alors, ça a commencé par un fichier de sous-titres manquant, ce qui est assez difficile pour voir ce film hongrois. Une curiosité sympa mais que je trouve très moyenne au final.

## Sweeney Todd : Le Diabolique Barbier de Fleet Street

Ce n'est pas une très bonne comédie musicale, il y a des idées, ça en fait trop, bref, un peu partagé entre des moments très chouettes et très bas de gamme.

## Lady Snowblood

Une des sources d'inspirations dans lesquelles Tarantino a tout piqué pour son Kill Bill. Et en plus ça raconte la suite alors que c'est moins long.

## Casablanca

Grosse décéption, j'ai du mal à comprendre pourquoi ce film est un tel mythe. Sans doute que c'était le cas à l'époque mais avec le contexte actuel, je le trouve bien mais pas excellent.

## Pee-Wee Big Adventure

Une histoire à la fois loufoque et touchante, j'ai passé un très bon moment.

## The Outsiders

Un excellent film, une très grande mise en scène, un vrai périple sur les deux protagonistes qui nous font ressentir ce qui leur arrive. C'est aussi un des premiers films où on voit Tom Cruise jeune qui ne sait pas manger du chocolat.

## Condenados a vivir

Un film qui a inspiré les 8 salopards de Tarantino. Il est pourtant assez mauvais sur son ensemble, trop long, pas très clair sur sa caractérisation de personnages.

## Yojimbo

C'est tellement bien rythmé, c'est tellement bien construit, à voir absolument.

## Time to love

J'ai étonnamment bien aimé ce film qui part d'un homme qui tombe amoureux du portrait d'une femme (et seulement son image et non la femme en question).

## Point limite

Un film sur la guerre froide dans une mise en scène impeccable avec des moments terribles. Et dire que ça a été retardé par Kubrick qui ne voulait pas que ça fasse de l'ombre à son Docteur Folamour.

## Un cas particulier

Je crois qu'on était deux à regarder la montre de nombreuses fois pour un film qui ne va nulle part. Seules les prestations d'acteurs sont correctes, le récit est catastrophique.

## Beetlejuice

Une déception pour ce film mythique pour des raisons similaires à celles de Frankenweenie.

## Mauvais sang

Alors il y a tellement d'idées en termes de plans, de montage que rien que pour ça, ça vaut le coup d'aller le voir. Cependant, c'est le genre de films qui méritent un second visionnage, je pense.

## Sleepy Hollow : La Légende du cavalier sans tête

J'ai passé un bon moment devant ce film qui n'est pas le meilleur film de son auteur mais pas son plus mauvais.

## Casque d'or

En terme de mise en scène et de scénario c'est bien mais c'est difficile de le sortir de son époque.

## Reds

Un très bon film autour de John Reed et la révolution Russe.

## Miss Peregrine et les enfants particuliers

J'ai bien aimé ce film de Tim Burton qui est à la limite du too much mais qui ne la franchit heureusement pas.

## Pinocchio

C'est très bien de voir un film que son cinéaste arrive à mettre dans son propre univers. J'espère que vous aimez le chocolat chaud.

## Scream

Oui, je n'avais jamais vu Scream, mais c'est génial, poue ceux qui comme moi ne l'ont pas vu, n'ayez pas peur, allez le voir.

## The Lost City of Z

Un beau film contemplatif de James Gray, un peu long mais bien plus intéressant sur l'exploration que ce qu'à pu faire Ridley Scott avec son Christophe Colomb.

## La Petite

J'avoue avoir été choqué par le film qui met en scène à hauteur d'enfant une fraction de la vie d'une petite fille qui est née dans une maison close.

## Big Fish

C'est un grand film de cinéma en terme de scénario, de mise en scène et de subtilité. Ça serait chouette de voir Tim Burton réaliser de nouveaux films aussi grandioses.